import os
import unittest

from the_ossuary.util.unique_id import dir_to_unique_id
from the_ossuary.util.unique_id import extract_unique_id_from_dir
from the_ossuary.util.unique_id import unique_id_to_dir


class TestUniqueId(unittest.TestCase):
    def test_dir_to_unique_id_01(self):
        path = "/this/is/my/prefix/be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e/"
        prefix = "/this/is/my/prefix"
        separator = '/'

        unique_id = dir_to_unique_id(path, prefix, separator)

        self.assertEqual(unique_id, 'be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e')

    def test_dir_to_unique_id_02(self):
        path = "/this/is/my/prefix/be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e"
        prefix = "/this/is/my/prefix"
        separator = '/'

        unique_id = dir_to_unique_id(path, prefix, separator)

        self.assertEqual(unique_id, 'be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e')

    def test_dir_to_unique_id_03(self):
        path = "/this/is/my/prefix/be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e"
        prefix = "/this/is/my/prefix/"
        separator = '/'

        unique_id = dir_to_unique_id(path, prefix, separator)

        self.assertEqual(unique_id, 'be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e')

    def test_dir_to_unique_id_04(self):
        # The prefix and the unique is path is not properly separated here, but is still matched.
        path = "/this/is/my/prefixbe/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e"
        prefix = "/this/is/my/prefix"
        separator = '/'

        unique_id = dir_to_unique_id(path, prefix, separator)

        self.assertEqual(unique_id, 'be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e')

    def test_extract_unique_id_from_dir_01(self):
        path = "/this/is/my/prefix/be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e"
        separator = '/'

        unique_id = extract_unique_id_from_dir(path, separator)

        self.assertEqual(unique_id, 'be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e')

    def test_extract_unique_id_from_dir_02(self):
        # The prefix and the unique is path is not properly separated here, but is still extracted.
        path = "/this/is/my/prefixbe/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e"
        separator = '/'

        unique_id = extract_unique_id_from_dir(path, separator)

        self.assertEqual(unique_id, 'be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e')

    def test_unique_id_to_dir_01(self):
        unique_id = "be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e"
        separator = "/"

        directory = unique_id_to_dir(unique_id, separator=separator)
        self.assertEqual(directory, "be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e")

    def test_unique_id_to_dir_02(self):
        unique_id = "be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e"
        prefix = "/this/is/my/prefix"
        separator = "/"

        directory = unique_id_to_dir(unique_id, prefix, separator=separator)
        self.assertEqual(directory, "/this/is/my/prefix/be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e")

    def test_unique_id_to_dir_03(self):
        unique_id = "be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e"
        prefix = "/this/is/my/prefix/"
        separator = "/"

        directory = unique_id_to_dir(unique_id, prefix, separator=separator)
        self.assertEqual(directory, "/this/is/my/prefix/be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e")

    def test_unique_id_to_dir_04(self):
        unique_id = "be75c8b7-3a2a-4b5c-82c0-d32f5ea4d03e"
        prefix = "/this/is/my/prefix////"
        separator = "/"

        directory = unique_id_to_dir(unique_id, prefix, separator=separator)
        self.assertEqual(directory, "/this/is/my/prefix/be/75/c8/b7/3a/2a/4b/5c/82/c0/d3/2f/5e/a4/d0/3e")