import os
import unittest

from the_ossuary.util.scp import retrieve_file
from the_ossuary.util.scp import RemoteFileNotFoundException


class TestScp(unittest.TestCase):

    def test_retrieve_file_01(self):
        retrieve_file("lnagy2", "ssh.geos.ed.ac.uk", "db_user.json", r"D:\data")
        self.assertTrue(os.path.isfile(r"D:\data\db_user.json"))
        os.remove(r"D:\data\db_user.json")

    def test_retrieve_file_02(self):
        self.assertRaises(RemoteFileNotFoundException,
                          retrieve_file, "lnagy2", "ssh.geos.ed.ac.uk", "nothing.json", r"D:\data")
