--
-- This is a file that will create an analytics table (_antbl_*) on the database that will show mgst values (materials,
-- geometry, size and temperatures) for models.
--

drop table if exists _antbl_model_mgst_rs;

drop index if exists _antbl_model_mgst_rs_index001;

create table _antbl_model_mgst_rs as
    select model.id as model_id,
           string_agg(material.name, ',' order by material.name) as materials,
           string_agg(cast(material.temperature as text), ',' order by material.name) as temperatures,
           model.geometry_id,
           geometry.name as geometry_name,
           cast(geometry.size as text) as geometry_size,
           running_status.name as running_status
    from
        model
        inner join model_material_association as mma
            on mma.model_id = model.id
        inner join material
            on mma.material_id = material.id
        inner join geometry
            on model.geometry_id = geometry.id
        inner join running_status
            on model.running_status_id = running_status.id
    group by
        model.id,
        model.geometry_id,
        geometry.name,
        geometry.size,
        running_status.name
;

create unique index _antbl_model_mgst_rs_index001 on _antbl_model_mgst_rs (model_id);
