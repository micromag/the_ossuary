--
-- This is a file that will create an analytics table (_antbl_*) on the database that will show mgst values (materials,
-- geometry, size and temperatures) for the start and end models associated with an NEB path. It will also display the
-- running status for the NEB.
--

drop table if exists _antbl_neb_mgst_rs;

drop index if exists _antbl_neb_mgst_rs_index001;

create table _antbl_neb_mgst_rs as
    select start_neb_id as neb_id,
           start_model_materials,
           start_model_temperatures,
           start_model_geometry_id,
           start_model_geometry_name,
           start_model_geometry_size,
           start_model_running_status,
           end_model_materials,
           end_model_temperatures,
           end_model_geometry_id,
           end_model_geometry_name,
           end_model_geometry_size,
           end_model_running_status,
           neb_running_status,
           neb_has_parent
    from (
         select neb.id                                                                       as start_neb_id,
                string_agg(smaterial.name, ',' order by smaterial.name)                      as start_model_materials,
                string_agg(cast(smaterial.temperature as text), ',' order by smaterial.name) as start_model_temperatures,
                smodel.geometry_id                                                           as start_model_geometry_id,
                sgeometry.name                                                               as start_model_geometry_name,
                sgeometry.size                                                               as start_model_geometry_size,
                srunning_status.name                                                         as start_model_running_status,
                running_status.name                                                          as neb_running_status,
                case when parent_neb_id is null then 'no' else 'yes' end                     as neb_has_parent
         from neb
                  inner join model as smodel on neb.start_model_id = smodel.id
                  inner join model_material_association as smma on smma.model_id = smodel.id
                  inner join material as smaterial on smma.material_id = smaterial.id
                  inner join geometry as sgeometry on smodel.geometry_id = sgeometry.id
                  inner join running_status as srunning_status on smodel.running_status_id = srunning_status.id
                  inner join running_status on neb.running_status_id = running_status.id
         group by neb.id,
                  smodel.geometry_id,
                  sgeometry.name,
                  sgeometry.size,
                  srunning_status.name,
                  running_status.name,
                  neb_has_parent
     ) as neb_start_data

    inner join (
        select neb.id                                                                       as end_neb_id,
               string_agg(ematerial.name, ',' order by ematerial.name)                      as end_model_materials,
               string_agg(cast(ematerial.temperature as text), ',' order by ematerial.name) as end_model_temperatures,
               emodel.geometry_id                                                           as end_model_geometry_id,
               egeometry.name                                                               as end_model_geometry_name,
               egeometry.size                                                               as end_model_geometry_size,
               erunning_status.name                                                         as end_model_running_status
        from neb
                 inner join model as emodel on neb.end_model_id = emodel.id
                 inner join model_material_association as emma on emma.model_id = emodel.id
                 inner join material as ematerial on emma.material_id = ematerial.id
                 inner join geometry as egeometry on emodel.geometry_id = egeometry.id
                 inner join running_status as erunning_status on emodel.running_status_id = erunning_status.id
        group by neb.id,
                 emodel.geometry_id,
                 egeometry.name,
                 egeometry.size,
                 erunning_status.name
    ) as neb_end_data
    on neb_start_data.start_neb_id = neb_end_data.end_neb_id
;

create unique index _antbl_neb_mgst_rs_index001 on _antbl_neb_mgst_rs (neb_id);