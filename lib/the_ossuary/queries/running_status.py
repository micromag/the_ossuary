

def get_running_status_info():
    r"""
    Construct a query to return all the running statuses on the system.
    :return: sql query
    """
    query = r"""
        select * from running_status
    """

    return query
