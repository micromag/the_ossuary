def get_model_unique_ids_by_mgst(materials, geometry, size, temperature):
    r"""
    Generate an SQL query string that will retrieve a list of model unique ids that match a given material, geometry
    size and temperature.

    :param materials: a python list of material names.
    :param geometry: a geometry name.
    :param size: a size (in micron).
    :param temperature: a temperature (in degrees centigrade).
    :return: a query string.
    """

    material_subqueries = []
    for material in materials:
        material_subquery = r"""
            ('{material:}', {temperature:}) in (
                SELECT material.name, material.temperature 
                FROM material
                    INNER JOIN model_material_association AS mma 
                        ON mma.material_id = material.id
                WHERE
                    mma.model_id = model.id
            )
        """.format(material=material, temperature=temperature)
        material_subqueries.append(material_subquery)

    query = r"""
        SELECT model.unique_id FROM model 
            INNER JOIN geometry ON model.geometry_id = geometry.id
        WHERE
            geometry.size = {size:}       AND 
            geometry.name = '{geometry:}' AND
            ({material_subqueries:})
    """.format(size=size, geometry=geometry, material_subqueries='OR'.join(material_subqueries))

    return query


def get_model_mgst_tuples(delimiter=";"):
    r"""
    Retrieve's all mgst (materials, geometry, size, temperatures) tuples from the database - these are semi-colon
    delimited. The materials and temperatures are comma delimited and each material/temperature comma-position
    correspond to each other.
    :param delimiter: the delimiter with which to separate mgst tuples.
    :return: an sql query
    """

    query = r"""
        select 
            distinct materials || '{delim:}' || geometry_name || '{delim:}' || geometry_size || '{delim:}' || temperatures as data
        from _antbl_model_mgst_rs
    """.format(delim=delimiter)

    return query


def assay_model_mgst_running_status(running_status, delimiter=";"):
    r"""
    Assay the running of a selection of models.
    :param running_status: the running status for which we would like to take an mgst count.
    :param delimiter: the delimiter with which to separate mgst tuples.
    :return: an sql query.
    """

    query = r"""
        select 
            materials || '{delim:}' || geometry_name || '{delim:}' || geometry_size || '{delim:}' || temperatures as data, 
            count(1) as count
        from _antbl_model_mgst_rs
        where
            running_status like '{status:}'
        group by
            materials,
            temperatures,
            geometry_name,
            geometry_size
    """.format(delim=delimiter, status=running_status)

    return query
