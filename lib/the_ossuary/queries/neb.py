def get_neb_unique_ids_by_mgst(materials, geometry, size, temperature, with_parent=True):
    r"""
    Generate an SQL query string that will retrieve a list of neb unique ids that match a given material, geometry
    size and temperature.

    :param materials: a python list of material names.
    :param geometry: a geometry name.
    :param size: a size (in micron).
    :param temperature: a temperature (in degrees centigrade).
    :param with_parent: a boolean flag indicating whether the retreived NEB ids should have parents.
    :return: a query string.
    """
    material_subqueries = []
    for material in materials:
        material_subquery = r"""
            ('{material:}', {temperature:}) in (
                SELECT material.name, material.temperature 
                FROM material
                    INNER JOIN model_material_association AS mma 
                        ON mma.material_id = material.id
                WHERE
                    mma.model_id = smod.id
            )
        """.format(material=material, temperature=temperature)
        material_subqueries.append(material_subquery)

    if with_parent:
        parent_clause = "neb.parent_neb_id IS NOT NULL "
    else:
        parent_clause = "neb.parent_neb_id IS NULL "

    query = r"""
        SELECT neb.unique_id FROM neb 
            INNER JOIN model as smod on smod.id = neb.start_model_id
            INNER JOIN geometry as sgeom on smod.geometry_id = sgeom.id
            INNER JOIN model as emod on emod.id = neb.end_model_id
            INNER JOIN geometry as egeom on emod.geometry_id = egeom.id
        WHERE
            sgeom.size = {size:}           AND
            sgeom.name = '{geometry:}'     AND
            {parent_clause:}  AND
        (
            {material_subqueries:}
        )
    """.format(size=size, geometry=geometry, material_subqueries='OR'.join(material_subqueries),
               parent_clause=parent_clause)

    return query


def get_neb_mgst_tuples(delimiter=";"):
    r"""
    Retrieve's all mgst (materials, geometry, size, temperatures) tuples from the database - these are semi-colon
    delimited. The materials and temperatures are comma delimited and each material/temperature comma-position
    correspond to each other.
    :param delimiter: the delimiter with which to separate mgst tuples.
    :return: an sql query
    """

    query = r"""
        select 
            distinct start_model_materials || '{delim:}' || start_model_geometry_name || '{delim:}' || start_model_geometry_size || '{delim:}' || start_model_temperatures || '{delim:}' || neb_has_parent as data
        from _antbl_neb_mgst_rs
    """.format(delim=delimiter)

    return query


def assay_neb_mgst_running_status(running_status, delimiter=";"):
    r"""
    Assay the running of a selection of models.
    :param running_status: the running status for which we would like to take an mgst count.
    :param delimiter: the delimiter with which to separate mgst tuples.
    :return: an sql query.
    """

    query = r"""
        select 
            start_model_materials || '{delim:}' || start_model_geometry_name || '{delim:}' || start_model_geometry_size || '{delim:}' || start_model_temperatures || '{delim:}' || neb_has_parent as data, 
            count(1) as count
        from _antbl_neb_mgst_rs
        where
            neb_running_status like '{status:}'
        group by
            start_model_materials,
            start_model_temperatures,
            start_model_geometry_name,
            start_model_geometry_size,
            neb_has_parent
    """.format(delim=delimiter, status=running_status)

    return query
