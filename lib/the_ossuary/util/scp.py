import os
import re

from platform import platform

from subprocess import Popen, PIPE


class UnsupportedPlatformException(Exception):
    def __init__(self, message):
        self.message = message


class DirectoryNotFoundException(Exception):
    def __init__(self, message):
        self.message = message


class RemoteFileNotFoundException(Exception):
    def __init__(self, message):
        self.message = message


def default_scp_command():
    platform_name = platform()
    if re.search("[Ww]indows", platform_name):
        return "pscp -P 22 -scp "
    else:
        raise UnsupportedPlatformException(platform_name)


def retrieve_file(user, host, remote_file_abs_path, local_directory):
    # Check that the local directory exists.
    if not os.path.isdir(local_directory):
        raise DirectoryNotFoundException(local_directory)

    # Build command that will be executed.
    cmd = "{scp:} {user:}@{host:}:{remote:} {dest_dir:}".format(
        scp=default_scp_command(),
        user=user,
        host=host,
        remote=remote_file_abs_path,
        dest_dir=local_directory
    )

    # Attempt to retrieve the file.
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)

    stdout, stderr = proc.communicate()

    if proc.returncode != 0:
        raise RemoteFileNotFoundException(remote_file_abs_path)


