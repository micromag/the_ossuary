import os
import re


def unique_id_to_dir(unique_id, prefix=None, separator=os.path.sep):
    r"""
    Converts a unique id to a path.
    :param unique_id: the unique id.
    :param prefix: prefix a path if required.
    :param separator: the directory separator (by default the os's separator is used).
    :return: a directory.
    """
    # Remove hyphens.
    uid = unique_id.replace('-', '')

    # Split in to pairs.
    n = 2
    uid_pairs = [uid[i:i+n] for i in range(0, len(uid), n)]

    # Clean up the prefix.
    p = prefix
    if p:
        while True:
            if p[-1] == separator:
                p = p[0:-1]
            else:
                break

    # Join to a path.
    if p:
        return separator.join([p]+uid_pairs)
    else:
        return separator.join(uid_pairs)


def dir_to_unique_id(directory, prefix=None, separator=os.path.sep):
    r"""
    Converts a directory to a unique id (the optional prefix is removed).
    :param directory: the directory.
    :param prefix: the prefix (by default None).
    :param separator: the directory separator (by default the os's separator is used).
    :return: unique_id if one could be extracted or None.
    """
    # Make sure to work with strings.
    d = str(directory)
    p = str(prefix)

    # Remove prefix.
    if prefix:
        d = d.replace(prefix, '')

    # If the first character is a separator, remove it.
    if d[0] == separator:
        d = d[1:]

    # If the last character is a separator, remove it.
    if d[-1] == separator:
        d = d[0:-1]

    # Split on the separator.
    components = d.split(separator)

    # Check that there is the correct number of components.
    if len(components) != 16:
        return None

    # Check each component is two characters long.
    for c in components:
        if len(c) != 2:
            return None

    unique_id = ''.join(components[0:4]) + '-' \
                + ''.join(components[4:6]) + '-' \
                + ''.join(components[6:8]) + '-' \
                + ''.join(components[8:10]) + '-' \
                + ''.join(components[10:20])

    # Check that unique_id matches the unique_id regex.
    if dir_to_unique_id.regex_unique_id.match(unique_id):
        return unique_id
    else:
        return None


dir_to_unique_id.regex_unique_id = re.compile(r"[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")


def extract_unique_id_from_dir(directory, separator=os.path.sep):
    r"""
    Attempt to extract a unique id from a directory.
    :param directory: the directory name to extract the unique id from.
    :param separator: the directory separator (by default the os's separator is used).
    :return: a unique id if one could be found or None.
    """
    # Make a regular expression.
    regex_unique_id = re.compile(
        "([0-9a-f]{{2}}({sep:}[0-9a-f]{{2}}){{15}})".format(
            sep=separator
        )
    )

    match_unique_id = regex_unique_id.search(directory)
    if match_unique_id:
        components = match_unique_id.group(0).split(separator)
        unique_id = ''.join(components[0:4]) + '-' \
                    + ''.join(components[4:6]) + '-' \
                    + ''.join(components[6:8]) + '-' \
                    + ''.join(components[8:10]) + '-' \
                    + ''.join(components[10:20])
        return unique_id
    else:
        return None
