r"""
This is a set of queries that will generate a bunch of 'hidden' tables that are useful for data analysis. Each analysis
table begins with _antbl_ followed by its name.
"""


def execute_sql_commands(conn, commands):
    r"""
    Execute an array of SQL commands.
    :param conn: the psycopg2 database connection.
    :param commands: the SQL commands to execute.
    :return: None
    """
    conn.set_session(readonly=False)
    cur = conn.cursor()
    for command in commands:
        cur.execute(command)
    cur.close()
    conn.commit()
    conn.set_session(readonly=True)


def create_antbl_model_mgst_rs(conn, destroy_first=True, delimiter=","):
    r"""
    These commands will create a table that contains summarized data for the number of models, it contains the model's
    materials (as a comma separated list), temperatures (as a comma separated list), geometry, size (in micron) and its
    running status.
    :param conn: a psycopg2 connection
    :param destroy_first: destroy objects associated with _antbl_model_mgst_rs prior to rebuilding (default: True)
    :param delimiter: the delimiter with which to separate materials and temperatures (for multi material models).
    :return: an sql query to construct _antbl_model_mgst_rs
    """

    if destroy_first:
        destroy_antbl_model_mgst_rs(conn)

    commands = [
        r"""
        create table _antbl_model_mgst_rs as
            select model.id as model_id,
                   string_agg(material.name, '{delim:}' order by material.name) as materials,
                   string_agg(cast(material.temperature as text), '{delim:}' order by material.name) as temperatures,
                   model.geometry_id,
                   geometry.name as geometry_name,
                   cast(geometry.size as text) as geometry_size,
                   running_status.name as running_status
            from
                model
                inner join model_material_association as mma
                    on mma.model_id = model.id
                inner join material
                    on mma.material_id = material.id
                inner join geometry
                    on model.geometry_id = geometry.id
                inner join running_status
                    on model.running_status_id = running_status.id
            group by
                model.id,
                model.geometry_id,
                geometry.name,
                geometry.size,
                running_status.name
        """.format(delim=delimiter),

        r"""
        create unique index _antbl_model_mgst_rs_index001 on _antbl_model_mgst_rs (model_id)
        """,

        r"""
        create        index _antbl_model_mgst_rs_index002 on _antbl_model_mgst_rs (materials,temperatures,geometry_name,geometry_size)
        """,

        r"""
        create        index _antbl_model_mgst_rs_index003 on _antbl_model_mgst_rs (running_status)
        """
    ]

    # Rebuild the analysis table here.
    execute_sql_commands(conn, commands)


def destroy_antbl_model_mgst_rs(conn):
    r"""
    These commands will destroy the antbl_model_mgst_rs table along with any indices.
    :param conn: the psycopg2 database connection.
    :return: None
    """
    commands = [
        r"""
        drop table if exists _antbl_model_mgst_rs
        """,

        r"""
        drop index if exists _antbl_model_mgst_rs_index001
        """,

        r"""
        drop index if exists _antbl_model_mgst_rs_index002
        """,

        r"""
        drop index if exists _antbl_model_mgst_rs_index003
        """
    ]

    execute_sql_commands(conn, commands)


def create_antbl_neb_mgst_rs(conn, destroy_first=True, delimiter=","):
    r"""
    These commands will create a table that contains summarized data for the number of nebs, it contains the nebs'
    materials (as a comma separated list), temperatures (as a comma separated list), geometry, size (in micron) and its
    running status, this is for both the start and end model (which should be the same in both cases).
    :param conn: the psycopg2 connection.
    :param destroy_first: destroy antbl_neb_mgst_rs prior to rebuilding? (default: True).
    :param delimiter: the delimiter with which to separate mgst tuples.
    :return: an sql query to construct _antbl_neb_mgst_rs
    """
    if destroy_first:
        destroy_antbl_neb_mgst_rs(conn)

    commands = [
        r"""
            create table _antbl_neb_mgst_rs as
                select start_neb_id as neb_id,
                       start_model_materials,
                       start_model_temperatures,
                       start_model_geometry_id,
                       start_model_geometry_name,
                       start_model_geometry_size,
                       start_model_running_status,
                       end_model_materials,
                       end_model_temperatures,
                       end_model_geometry_id,
                       end_model_geometry_name,
                       end_model_geometry_size,
                       end_model_running_status,
                       neb_running_status,
                       neb_has_parent
                from (
                     select neb.id                                                                       as start_neb_id,
                            string_agg(smaterial.name, '{delim:}' order by smaterial.name)                      as start_model_materials,
                            string_agg(cast(smaterial.temperature as text), '{delim:}' order by smaterial.name) as start_model_temperatures,
                            smodel.geometry_id                                                           as start_model_geometry_id,
                            sgeometry.name                                                               as start_model_geometry_name,
                            sgeometry.size                                                               as start_model_geometry_size,
                            srunning_status.name                                                         as start_model_running_status,
                            running_status.name                                                          as neb_running_status,
                            case when parent_neb_id is null then 'no' else 'yes' end                     as neb_has_parent
                     from neb
                              inner join model as smodel on neb.start_model_id = smodel.id
                              inner join model_material_association as smma on smma.model_id = smodel.id
                              inner join material as smaterial on smma.material_id = smaterial.id
                              inner join geometry as sgeometry on smodel.geometry_id = sgeometry.id
                              inner join running_status as srunning_status on smodel.running_status_id = srunning_status.id
                              inner join running_status on neb.running_status_id = running_status.id
                     group by neb.id,
                              smodel.geometry_id,
                              sgeometry.name,
                              sgeometry.size,
                              srunning_status.name,
                              running_status.name,
                              neb_has_parent
                 ) as neb_start_data
            
                inner join (
                    select neb.id                                                                       as end_neb_id,
                           string_agg(ematerial.name, '{delim:}' order by ematerial.name)                      as end_model_materials,
                           string_agg(cast(ematerial.temperature as text), '{delim:}' order by ematerial.name) as end_model_temperatures,
                           emodel.geometry_id                                                           as end_model_geometry_id,
                           egeometry.name                                                               as end_model_geometry_name,
                           egeometry.size                                                               as end_model_geometry_size,
                           erunning_status.name                                                         as end_model_running_status
                    from neb
                             inner join model as emodel on neb.end_model_id = emodel.id
                             inner join model_material_association as emma on emma.model_id = emodel.id
                             inner join material as ematerial on emma.material_id = ematerial.id
                             inner join geometry as egeometry on emodel.geometry_id = egeometry.id
                             inner join running_status as erunning_status on emodel.running_status_id = erunning_status.id
                    group by neb.id,
                             emodel.geometry_id,
                             egeometry.name,
                             egeometry.size,
                             erunning_status.name
                ) as neb_end_data
                on neb_start_data.start_neb_id = neb_end_data.end_neb_id
        """.format(delim=delimiter),

        r"""
        create unique index _antbl_neb_mgst_rs_index001 on _antbl_neb_mgst_rs (neb_id)
        """,

        r"""
        create        index _antbl_neb_mgst_rs_index002 on _antbl_neb_mgst_rs(start_model_materials,start_model_temperatures,start_model_geometry_name,start_model_geometry_size)
        """,

        r"""
        create        index _antbl_neb_mgst_rs_index003 on _antbl_neb_mgst_rs(end_model_materials,end_model_temperatures,end_model_geometry_name,end_model_geometry_size)
        """,

        r"""
        create        index _antbl_neb_mgst_rs_index004 on _antbl_neb_mgst_rs(neb_running_status)
        """
    ]

    execute_sql_commands(conn, commands)


def destroy_antbl_neb_mgst_rs(conn):
    commands = [
        r"""
        drop table if exists _antbl_neb_mgst_rs
        """,

        r"""
        drop index if exists _antbl_neb_mgst_rs_index001
        """,

        r"""
        drop index if exists _antbl_neb_mgst_rs_index002
        """,

        r"""
        drop index if exists _antbl_neb_mgst_rs_index003
        """,

        r"""
        drop index if exists _antbl_neb_mgst_rs_index004
        """
    ]

    execute_sql_commands(conn, commands)
