import psycopg2


def postgres_connection(dbname, user, host, password=None):
    r"""
    Retriev a postgres connection.
    :param dbname: database name.
    :param user: database user name.
    :param host: database host machine.
    :param password: password (optional).
    :return: a valid connection or an exception is raised.
    """
    if password:
        connection = psycopg2.connect(
            "dbname='{dbname:}' user='{user:}' host='{host:}' password='{password:}'".format(
                dbname=dbname, user=user, host=host, password=password
            )
        )
        return connection
    else:
        connection = psycopg2.connect(
            "dbname='{dbname:}' user='{user:}' host='{host:}'".format(
                dbname=dbname, user=user, host=host
            )
        )
        return connection


